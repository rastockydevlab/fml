// Utilities
import { defineStore } from "pinia";

export const useAppStore = defineStore("app", {
  state: () => ({
    pages: [],
    datasets: [],
    activePage: undefined,
    activePageUi: undefined,
  }),
  actions: {
    async getAThingById(id) {
      let response = await fetch(
        `${import.meta.env.VITE_API_URL}/things?id=${id}`
      );

      const data = await response.json();
      return data[0];
    },
    async getThingsBelongingToId(id) {
      let response = await fetch(
        `${import.meta.env.VITE_API_URL}/things?parent=${id}`
      );

      const data = await response.json();
      return data;
    },
    async loadPages() {
      this.pages = await this.getThingsBelongingToId(2);
    },
    async loadDatasets() {
      this.datasets = await this.getThingsBelongingToId(1);
    },
    async loadActivePage(id) {
      this.activePage = await this.getAThingById(id);
    },
    async loadActivePageUi(id) {
      this.activePageUi = await this.getThingsBelongingToId(id);
    },
  },
});

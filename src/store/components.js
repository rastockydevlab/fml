// Utilities
import { defineStore } from "pinia";

export const useComponentsStore = defineStore("components", {
  state: () => ({}),
});

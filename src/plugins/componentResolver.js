import AText from "@/components/widgets/AText.vue";
import AButton from "@/components/widgets/AButton.vue";
import ATable from "@/components/widgets/ATable.vue";
import ACheckbox from "@/components/widgets/ACheckbox.vue";

import NotImplemented from "@/components/widgets/NotImplemented.vue";

export default {
  install: (app) => {
    app.config.globalProperties.$resolveComponent = (componentName) => {
      const components = {
        AText,
        AButton,
        ATable,
        ACheckbox,
      };

      const component = components[componentName];

      if (component) {
        return component;
      }

      return NotImplemented;
    };
  },
};
